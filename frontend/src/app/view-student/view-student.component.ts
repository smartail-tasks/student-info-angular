import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css']
})
export class ViewStudentComponent {
  student:any;
  id:String|null='';
  constructor(private apiService: ApiService, private route: ActivatedRoute){
    this.route.paramMap.subscribe(params=>{
      this.id=params.get("id");
      console.log("params: ",this.id);
    })

    this.apiService.getStudent(this.id).subscribe({
      next:((res:any)=>{
        this.student=res.student;
        console.log("student: ",this.student);
      }),
      error:(err=>{
        console.log("error: ",err);
      })
    })
  }

}