import { Component } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  data:any;
  constructor(private apiService:ApiService,private router:Router) {
    this.apiService.getStudents().subscribe({
      next:(data:any)=>{
        console.log(data);
        this.data=data.students;
        console.log(this.data);
      },
      error:(err)=>{
        console.log(err);
      }
    })
  }

  handleEditClick(id:any){
    console.log(id);
    this.router.navigateByUrl(`/updatestudent/${id}`);
  }

  handleViewClick(id:any){
    console.log(id);
    this.router.navigateByUrl(`/viewstudent/${id}`);
  }

  handleDeleteClick(id:any){
    console.log(id);
    if(confirm(`Are you sure to Delete ${id}?`)){
      console.log('yes');
      this.apiService.deleteStudent(id).subscribe({
        next:(res=>{
          console.log(res);
          window.location.reload();
        }),
        error:(err=>{
          console.log(err);
        })
      });
    }
    else{
      console.log('no');
    }
  }

}
