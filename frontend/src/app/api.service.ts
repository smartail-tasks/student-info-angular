import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private URL = 'http://127.0.0.1:8080'

  constructor(private httpClient:HttpClient) { }

  public getStudents(){
    return this.httpClient.get(this.URL+'/students');
  }

  public getStudent(id:any){
    return this.httpClient.get(this.URL+`/student/${id}`);
  }

  public addStudent(data:any){
    return this.httpClient.post(this.URL+'/addstudent',data);
  }

  public updateStudent(id:any,data:any){
    return this.httpClient.put(this.URL+`/student/${id}`,data);
  }

  public deleteStudent(id:any){
    return this.httpClient.delete(this.URL+`/student/${id}`);
  }

  public loginStudent(data:any){
    return this.httpClient.post(this.URL+"/login",data);
  }

}
