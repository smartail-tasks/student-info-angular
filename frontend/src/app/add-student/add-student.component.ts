import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent {
  myForm: FormGroup;

  constructor(private apiService:ApiService, private router:Router) {
    this.myForm = new FormGroup({
      id: new FormControl(''),
      name: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      program: new FormControl(''),
      phone: new FormControl('')
    });
  }

  onSubmit() {
    const userInput = this.myForm.value;
    console.log(userInput);
    this.apiService.addStudent(userInput).subscribe({
      next: (res:any)=>{
        console.log(res);
        alert(res.Message);
        this.router.navigateByUrl("/");
      },
      error: (err)=>{
        console.error(err);
        alert(err.error.Message);
      }
    });
  }
}
