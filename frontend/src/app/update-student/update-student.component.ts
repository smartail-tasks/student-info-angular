import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent {
  myForm: FormGroup;
  id:String| null='';
  constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router){
    this.route.paramMap.subscribe(params=>{
      this.id=params.get("id");
      console.log("params: ",this.id);
    })
    this.myForm = new FormGroup({
      name: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      program: new FormControl(''),
      phone: new FormControl('')
    });

    this.apiService.getStudent(this.id).subscribe({
      next: (student:any)=>{
        this.myForm.setValue({name:student.student.name,email:student.student.email,password:student.student.password,program:student.student.program,phone:student.student.phone});
        console.log("student: ",student.student.email);
      },
      error: (err:any)=>{
        console.log(err);
      }
    });
  }

  onSubmit(){
    console.log(this.myForm.value);
    this.apiService.updateStudent(this.id,this.myForm.value).subscribe({
      next:(res=>{
        console.log(res);
        this.router.navigateByUrl('/');
      }),
      error:(err=>{
        console.log(err);
      })
    })
  }

}
