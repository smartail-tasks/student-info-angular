import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  myForm: FormGroup;

  constructor(private apiService: ApiService,private router: Router) {
    this.myForm = new FormGroup({
      id: new FormControl(''),
      password: new FormControl(''),
    });
  }

  onSubmit(){
    console.log(this.myForm.value);
    this.apiService.loginStudent(this.myForm.value).subscribe({
      next:((res:any)=>{
        console.log(res);
        alert(res.Message);
        this.router.navigateByUrl("/");
      }),
      error:err=>{
        console.log(err);
        alert(err.error.Message);
      }
    })
  }
}
