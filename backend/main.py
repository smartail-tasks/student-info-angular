from flask import Flask,request
from flask_cors import CORS, cross_origin

import pymongo


client = pymongo.MongoClient("mongodb+srv://vignaraj:vignu1018@mycluster.fgver9t.mongodb.net/?retryWrites=true&w=majority")
db = client['MyDatabase']
mycol=db['student-info']

student_data_format={'_id': '', 'name': '', 'phone': '', 'email': '', 'password': '', 
              'program': '', 'attendance': {'present_days': 0, 'absent_days': 0, 'overall_percentage': 0.0 }}

app = Flask(__name__)
CORS(app, support_credentials=True)

@cross_origin(supports_credentials=True)
@app.route("/")
def hello_world():
    return "Hello, World!"

@cross_origin(supports_credentials=True)
@app.route("/addstudent", methods=["POST"])
def add_student():
    d=request.get_json()
    print(d)
    existing_student = mycol.find_one({'_id': d['id']})
    if existing_student:
        return {"success":False,"Message":"User Already Registered"},409
    data=student_data_format.copy()
    data['_id']=d['id']
    data['name']=d['name']
    data['phone']=d['phone']
    data['email']=d['email']
    data['password']=d['password']
    data['program']=d['program']
    print(data)
    print(student_data_format)

    result=mycol.insert_one(data)
    print(result.inserted_id)

    return {"success":True,"_id":result.inserted_id,"Message":"User Registered Successfully"},201

@cross_origin(supports_credentials=True)
@app.route("/login", methods=["POST"])
def login_student():
    d=request.get_json()
    existing_student = mycol.find_one({'_id': d['id']})
    if not existing_student:
        return {"success":False,"Message":"User Not Found"},404
    elif d['password']!=existing_student['password']:
        return {"success":False,"Message":"Password Incorrect"},401
    else:
        return {"success":True,"Message":"Login Successfull"},200
    

@cross_origin(supports_credentials=True)
@app.route("/students", methods=["GET","POST"])
def get_students():
    res = [student for student in mycol.find()]
    if res:
        return {"success":True,"Message":"Fetched All Students","students":res},200
    else:
        return {"success":False,"Message":"Empty Database"},200

@cross_origin(supports_credentials=True)
@app.route("/student/<id>", methods=["GET","POST"])
def get_student(id):
    res = mycol.find_one({'_id': id})
    if res:
        return {"success":True,"Message":"Fetched Student Data","student":res},200
    else:
        return {"success":False,"Message":"Student Not Found"},404
    
@cross_origin(supports_credentials=True)
@app.route("/student/<id>", methods=["DELETE"])
def delete_student(id):
    result = mycol.delete_one({'_id': id})
    if result.deleted_count == 0:
        return {"Success":False,'Message': 'Student not found.'}, 404
    else:
        return {"Success":True,'Message': 'Student deleted successfully.'}, 200
    
@cross_origin(supports_credentials=True)
@app.route("/student/<id>", methods=["PUT"])
def update_student(id):
    d=request.get_json()
    
    existing_student = mycol.find_one({'_id': id})
    if not existing_student:
        return {"Success":False,'Message': 'Student not found.'}, 404
    else:
        if 'id' in d:
            del d['id']
        mycol.update_one({"_id": id}, {"$set": d})
        return {"Success":True,'Message': 'Student Data Updated.'}, 200

app.run(debug=True,host='0.0.0.0',port=8080)